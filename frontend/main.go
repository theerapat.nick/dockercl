package main

import (
	"html/template"
	"io"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	t := &Template {
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}
	e.Renderer = t
	e.GET("/", indexGet)
	e.POST("/", indexPost)
	e.Logger.Fatal(e.Start(":4000"))
}

type Template struct{
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}