//+build integration

package main

import "testing"

func TestParseXY(t *testing.T) {

	tables := []struct{
		x		string
		y 		string
		Expect 	string
	}{
		{"1", "2", "3"},
		{"1", "2", "3"},
	}

	for _, table := range tables {
		value := callBackEndSUM(table.x, table.y)
		if value != table.Expect {
			t.Errorf("Error %s %s -- %s %s", value, table.Expect, table.x, table.y)	
		}
	}
}