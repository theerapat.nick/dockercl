package main

import (
	"net/http"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	e.GET("/", index)
	e.GET("/sum", sum)

	e.Logger.Fatal(e.Start(":5000"))
}

func index(c echo.Context) error {
	return c.String(http.StatusOK, "Backend, Hello, World!")
}