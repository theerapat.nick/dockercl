package main

import "testing"

func TestSUMXY(t *testing.T) {
	tables := []struct {
		x		int
		y		int
		Expect 	int
	}{
		{10, 10, 20},
		{5, 5, 10},
	}

	for _, table := range tables {
		sumValue := sumXY(table.x, table.y)

		if sumValue != table.Expect {
			t.Errorf("Error %d %d -- %d %d", table.x, table.y, sumValue, table.Expect)
		}
	}
}